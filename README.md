# Gestion-compte dockerised

[Gestion-compte](https://github.com/elefan-grenoble/gestion-compte) est une
application web de gestion des membres et du bénévolat pour les supermarchés et
épiceries coopératives développée par les membres de [Éléfàn](https://lelefan.org)
à Grenoble.

Ce projet a pour but de proposer une version dockerisée de cette application.

## TODO

- [ ] Ajouter CRON
- [ ] Ajouter mailhog pour dev

## README

- [English](./README.en_GB.md)
- [Français](./README.md)

## Prerequis

- [Docker](https://www.docker.com/)
- [Make](https://www.gnu.org/software/make/manual/make.html) (optionel)

## Configuration

- Copiez le fichier de `app/parameters.yml.dist` :
  ``` bash
  cp app/parameters.yml.dist app/parameters.yml
  ```
- Modifiez le selon vos besoins.
  * ℹ️Pour tester en local, pensez à modifier
  `router.request_context.host` avec la valeur `localhost`
- Modifiez `db/.env` (attention à utiliser le même mot de passe pour la base de
  données dans `app/parameters.yml`)
- optionel - modifiez `web/gestion-compte.conf` selon vos besoins

## Installation

Le plus simple est d'utiliser `make`:
``` bash
make install
```

⚠️ Attention, cette opération
remet à zéro l'environnement docker : toute données précédemment présente sera
supprimée.

ℹ️ Notez que vous devrez valider 2 questions par `y` lors de l'installation.

Alternativement, vous pouvez lancer les commandes à la main :
``` bash
# Lance les containers docker et le build du container contenant l'app
docker-compose up -d --build

# Attendez 30 secondes que la base de données soit lancée et initialisée

# Lance le script dploy.sh qui déploie et initialise la version choisie de l'app
# Voir les tags : https://github.com/elefan-grenoble/gestion-compte/tags
# Notez que le script échoue à redémarrer PHP à la fin mais ceci est sans incidence
docker-compose exec -u root app /bin/bash dploy.sh v1.24

# Lance la migration de la base de données pour initialiser les tables
docker-compose exec -e SYMFONY_ENV=prod app php bin/console doctrine:migration:migrate

# Créé le compte administrateur de l'app
curl -sL http://localhost:8080/user/install_admin
```

## Initialisation

Suivez la [procédure fournie par les développeurs de l'application](https://github.com/elefan-grenoble/gestion-compte/blob/develop/doc/start.md).

ℹ️ Notez que les actions Symfony peuvent être lancée avec `make` :
``` bash
# Lance l'action `fos:user:activate mon-utilisateur`
make app.run ACTION="fos:user:activate mon-utilisateur"
```

## ⚠️ Avertissement ⚠️

Mes connaissances en PHP, Docker, NGINX et administration système sont limitées.
Malgré le soin apporté, il est très probable que ce projet soit sous optimal en
terme de performance, voire qu'il contienne des failles de sécurité.
