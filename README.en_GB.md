# Dockerised Gestion-compte

[Gestion-compte](https://github.com/elefan-grenoble/gestion-compte) is a web app
used to manage members and volunteering work for cooperative supermarkets and
grocery shops, developed by members of [Éléfàn](https://lelefan.org) in Grenoble,
France.

This project proposes a dockerised version of this application.

## README

- [English](./README.en_GB.md)
- [Français](./README.md)

## Prerequisite

- [Docker](https://www.docker.com/)
- [Make](https://www.gnu.org/software/make/manual/make.html) (optional)

## Configuration

- Copy the `app/parameters.yml.dist` file:
  ``` bash
  cp app/parameters.yml.dist app/parameters.yml
  ```
- Modify it according to your needs
  * ℹ️ For testing locally, remember to modify
  `router.request_context.host` with `localhost` as a value
- Modify `db/.env` (be careful about using the same password as in
  `app/parameters.yml` for the database)
- optional - modify `web/gestion-compte.conf` to your needs

## Install

The simplest is to use `make`:
``` bash
make install
```

⚠️ Be aware that this operation resets
the docker environment: all data will be erased.

ℹ️ Note that you will have to validate to questions with `y` during install.

Alternatively, you can run these commands manually :
``` bash
# Starts the docker containers and builds the app container
docker-compose up -d --build

# Wait 30 seconds for the database to start and initialise

# Run the dploy.sh script which deploys and initialise the chosen version of the app.
# See available tags : https://github.com/elefan-grenoble/gestion-compte/tags
# Note that the script fails to restart PHP at the end but is has no negative impact
docker-compose exec -u root app /bin/bash dploy.sh v1.24

# Run the database migration to create the tables
docker-compose exec -e SYMFONY_ENV=prod app php bin/console doctrine:migration:migrate

# Create the app admin user
curl -sL http://localhost:8080/user/install_admin
```

## Initialisation

Follow [the procedure created by the devs of the app](https://github.com/elefan-grenoble/gestion-compte/blob/develop/doc/start.md).

ℹ️ Note that Symfony actions can be run with `make`:
``` bash
# Run the `fos:user:activate mon-utilisateur` action
make app.run ACTION="fos:user:activate mon-utilisateur"
```

## ⚠️ Disclaimer ⚠️

My PHP, Docker, NGINX and system administration knowledge are limited. Despite
the attention, it is likely that this project is sub optimal in terms of
performance, it may even contain security flaws.
