VERSION=v1.24
COLOR=\033[0;95m
NC=\033[0m

default: help

stop: ## Stop the stack
	@echo "\n${COLOR} • Stopping containers ${NC}"
	docker-compose down

start: ## Start the stack
start: stop
	@echo "\n${COLOR} • Starting containers ${NC}"
	docker-compose up -d --build

clean: ## Destroy all containers and volumes. /!\ Destructive action /!\.
	@echo "\n${COLOR} • Destroying containers and volumes ${NC}"
	docker-compose down -v --remove-orphans
	docker-compose rm -sv

install: ## Install the entire stack. /!\ Erase all exising data /!\.
install: clean start db.wait app.deploy db.migrate app.install-admin

app.run: ## Run a Symfony command (php bin/console command), usage: make run ACTION=doctrine:migration:migrate
	@echo "\n${COLOR} • Running $(ACTION) ${NC}"
	docker-compose exec -e SYMFONY_ENV=prod app php bin/console $(ACTION)

app.deploy: ## Use the dploy.sh script to initialise the app. Version can be specified: make app.deploy VERSION=v1.0
	@echo "\n${COLOR} • Running provided dploy.sh script to deploy $(VERSION) ${NC}"
	-docker-compose exec -u root app /bin/bash dploy.sh $(VERSION)

app.install-admin: ## Install the admin user
	curl -sL http://localhost:8080/user/install_admin > /dev/null

db.migrate: ## Run database migration scripts (doctrine:migration:migrate)
db.migrate: ACTION=doctrine:migration:migrate
db.migrate: app.run

db.wait: ## Wait for the database to ready (started and database created)
	@printf "\n${COLOR} • Waiting for database to be ready" ; \
	until test "`docker inspect --format='{{json .State.Health.Status}}' gestion-compte.db`" = '"healthy"' ; \
	do printf "." ; sleep 3 ; done; \
	printf "✓ ${NC}\n";

help: ## Show this help message.
	@echo 'usage: make [target] ...'
	@echo ''
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s '#'

.PHONY: default help stop start clean install app.run app.deploy app.install-admin db.wait db.migrate
